angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.hearing'
      2) Using $state.go programatically:
        $state.go('tabsController.hearing');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/page2
      /page1/tab2/page2
  */
  .state('tabsController.hearing', {
    url: '/page2',
    views: {
      'tab1': {
        templateUrl: 'templates/hearing.html',
        controller: 'hearingCtrl'
      },
      'tab2': {
        templateUrl: 'templates/hearing.html',
        controller: 'hearingCtrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.hearing2'
      2) Using $state.go programatically:
        $state.go('tabsController.hearing2');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/page6
      /page1/tab2/page6
  */
  .state('tabsController.hearing2', {
    url: '/page6',
    views: {
      'tab1': {
        templateUrl: 'templates/hearing2.html',
        controller: 'hearing2Ctrl'
      },
      'tab2': {
        templateUrl: 'templates/hearing2.html',
        controller: 'hearing2Ctrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.hearing3'
      2) Using $state.go programatically:
        $state.go('tabsController.hearing3');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/page7
      /page1/tab2/page7
  */
  .state('tabsController.hearing3', {
    url: '/page7',
    views: {
      'tab1': {
        templateUrl: 'templates/hearing3.html',
        controller: 'hearing3Ctrl'
      },
      'tab2': {
        templateUrl: 'templates/hearing3.html',
        controller: 'hearing3Ctrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='tabsController.speaking'
      2) Using $state.go programatically:
        $state.go('tabsController.speaking');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page1/tab1/page3
      /page1/tab2/page3
  */
  .state('tabsController.speaking', {
    url: '/page3',
    views: {
      'tab1': {
        templateUrl: 'templates/speaking.html',
        controller: 'speakingCtrl'
      },
      'tab2': {
        templateUrl: 'templates/speaking.html',
        controller: 'speakingCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('login', {
    url: '/page4',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('signup', {
    url: '/page5',
    templateUrl: 'templates/signup.html',
    controller: 'signupCtrl'
  })

  .state('pairing', {
    url: '/page8',
    templateUrl: 'templates/pairing.html',
    controller: 'pairingCtrl'
  })

  .state('account', {
    url: '/page9',
    templateUrl: 'templates/account.html',
    controller: 'accountCtrl'
  })

  .state('settings', {
    url: '/page10',
    templateUrl: 'templates/settings.html',
    controller: 'settingsCtrl'
  })

$urlRouterProvider.otherwise('/page4')


});